# Document

## How to build:
cd LEMPhp7.0
docker build -t lemphp7.0
.
## Used pre-build image from DockerHub
docker pull phulerock/lemphp7.0
## How to run
docker run --name lemp7test -v /var/lib/jenkins/workspace/LEMPhp7.0/LEMPhp7.0/webapp:/var/www/html

