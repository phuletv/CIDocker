# UBUNTU LAMP docker
This image is build for development environment, not for production env by add some tools like ssh, telnet .. (please see my Dockfile)
based: ubuntu 16.04
app: mysql, php 7.0.15, sshd, apache2, supervisord
This Dockerfile is build and commit to Docker Hub repo phulerock/lamp

## Run and use:
`docker run --name container_name -d phulerock/lamp`
or build your own from this Dockerfile.

This container already